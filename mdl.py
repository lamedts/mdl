#!/usr/bin/env python2
# -*- coding: utf8 -*-

import sys, getopt

from utils import console
from utils import writeTag
from xiami import xiami

def usage():
    print "Description:"
    print "\tA downloader the song from some website"
    print "\nUsage:"
    print "\t_xiami.py [OPTION]... ID"
    print "\nOptions:"
    print "\t-h, --help           print this help"
    print "\t-s, --song           Download a  song by id"
    print "\t-a, --album          Download an album by id"
    print "\t-c, --collection     Download a  collection by id"

def main(argv):
    flag = False
    console.cls()
    """
    try:
        opts, args = getopt.getopt(argv,"ha:c:s:",["help","album=","collection=", "song="])
    except getopt.GetoptError:
        print ("getopt error!")
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            usage()
            flag = True
            sys.exit()
        elif opt in ("-a", "--album"):
            dlType = 0    #  "dlType" 0 => album, 1 => collection, 2 => song
            json_url = "http://www.xiami.com/song/playlist/id/%s/type/1/cat/json?_ksTS=1423711851040_1429" % arg
        elif opt in ("-c", "--collection"):
            dlType = 1    #  "dlType" 0 => album, 1 => collection, 2 => song
            json_url = "http://www.xiami.com/song/playlist/id/%s/type/3/cat/json?_ksTS=1422948834823" % arg
        elif opt in ("-s", "--song"):
            dlType = 2    #  "dlType" 0 => album, 1 => collection, 2 => song
            json_url = "http://www.xiami.com/song/playlist/id/%s/object_name/default/object_id/0/cat/json?_ksTS=1423751499433_1209" % arg
    if flag == False:
        #print json_url
        #xiami.get_song_json(xiami.set_dir(), json_url)
    """
    #
    dlargs = ""
    try:
        opts, args = getopt.getopt(argv,"hxnza:c:s:",["help","xiami","netmusic","zhcn","album=","collection=", "song="])
    except getopt.GetoptError:
        print ("getopt error!")
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            usage()
            flag = True
            sys.exit()
        elif opt in ("-a", "--album"):
            #dlType = 0    #  "dlType" 0 => album, 1 => collection, 2 => song
            
            tid = args
        elif opt in ("-c", "--collection"): 
            dlType = 1    #  "dlType" 0 => album, 1 => collection, 2 => song
            tid = args
        elif opt in ("-s", "--song"):
            dlType = 2    #  "dlType" 0 => album, 1 => collection, 2 => song
            tid = args
        elif opt in ("-x", "--xiami"):
            source = 0
        elif opt in ("-n", "--netmusic"):
            source = 1
        elif opt in ("-z", "--zhcn"):
            path = "D:/CloudMusic/林子祥/林子祥 - 改變常改變.mp3"
            path = unicode(path, "utf-8");
            writeTag.writeTag(path)
    if flag == False:
        print "-"
        #xiami.get_song_json(xiami.set_dir(), json_url)

if __name__ == "__main__":
   main(sys.argv[1:])