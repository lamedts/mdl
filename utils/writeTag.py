#!/usr/bin/env python2
# -*- coding: utf8 -*-

import os, sys

from mutagen.id3 import ID3,TRCK,TIT2,TALB,TPE1,APIC,TDRC,COMM,TPOS,USLT,ID3NoHeaderError

from utils.langconv import *

def writeTag(filepath, artist, album, song, num):
	try:
		tags = ID3(filepath)
	except ID3NoHeaderError:
		tags = ID3()
	tags.add(TIT2(encoding=3, text= song))
	tags.add(TALB(encoding=3, text= album))
	tags.add(TPE1(encoding=3, text= artist))
	tags.add(TRCK(encoding=3, text= str(num)))
	tags.save(filepath, v2_version=3)

def writeTag(folderpath):
	sys.stdout.write(u"%s\n\n" % folderpath)
	folderpath = "D:/CloudMusic/mp3/"
	print list(os.walk(u"D:/CloudMusic/mp3/")) 
	for filename in os.listdir(u"D:/CloudMusic/mp3/"):
		filepath = folderpath + filename
		print type(filename)
	#	print filepath.decode('utf-8')
		audio = ID3(filepath)
		#print type(audio['TIT2'].text[0])
		#print audio['TIT2'].text[0]
		#audio['TIT2'].text[0]
		#print Converter('zh-hant').convert(audio['TIT2'].text[0])
		#print filepath.encode('utf-8')
		#print filepath.decode('utf8')
		#metadata = audio.pprint()
		#sys.stdout.write(u"%s\n\n" % Converter('zh-hant').convert(audio['TPE1']))
		#sys.stdout.write(u"%s\n\n" % audio['TPE1'])
		#print Converter('zh-hant').convert(audio['TPE1']).text
		#print audio['TPE1'].text
		audio.add(TIT2(encoding=3, text= Converter('zh-hant').convert(audio['TIT2'][0])))		#song
		audio.add(TALB(encoding=3, text= Converter('zh-hant').convert(audio['TALB'][0])))		#album
		audio.add(TPE1(encoding=3, text= Converter('zh-hant').convert(audio['TPE1'][0])))		#artist
		audio.save(filepath, v2_version=3)
		os.renames(filepath, Converter('zh-hant').convert(filepath))
	
		